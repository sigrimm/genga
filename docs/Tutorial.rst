GENGA Tutorial
==============

Learn how to install and run GENGA in Google Colab `with this tutorial <https://gist.github.com/sigrimm/93e2faed18e0e39e82aa226097b78e2c>`_ .
Open the notebook file in Google Colab and follow the tutorial step by step.


